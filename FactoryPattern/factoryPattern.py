#!/usr/bin/env python
# encoding: utf-8
__author__ = 'Zjay '
"""
@version: 3.6.3
@author: Zjay 
@license: Apache Licence 
@site: 
@software: PyCharm
@file: factoryPattern.py
@time: 2018/3/28 21:21
"""

#region
# import xml.etree.ElementTree as etree;
# import json;
#
# #定义实体类读取Json文件数据
# class JSONConnector(object):
#     def __init__(self,path):
#         self.data =dict();
#         f= open(path,mode='r', encoding='utf-8');
#         self.data =json.loads(f);
#
#     @property
#     def GetData(self):
#         return self.data;
#
# class XMLConnector(object):
#     def __init__(self,filepath):
#         self.tree=etree.parse(filepath)
#
#     @property
#     def GetData(self):
#         return self.tree
#
# def  connector_factory(filepath):
#     if filepath.endswith('json'):
#          connector= JSONConnector
#     elif filepath.endswith('xml'):
#         connector = XMLConnector
#     else:
#         raise Exception('Error')
#     return connector(filepath)
#
# def main(filepath):
#     factory =None
#     try:
#         factory =connector_factory(filepath)
#     except Exception as ex:
#         print(ex)
#     return factory
#
#
# if  __name__ == '__main__':
#     xmlfactory = main('D:/Project/Project_Git/Python_DesignPatterns/FactoryPattern/person.xml')
#     jsonfactory = main('D:/Project/Project_Git/Python_DesignPatterns/FactoryPattern/donut.json')

#endregion

class Frog:
    def __init__(self,name):
        self.name=name

    def __str__(self):
        return self.name

    def interact_with(self,obstacle):
        print('\n\t {} the Frog encounters {} and {}!'.format(self,obstacle, obstacle.action()))

class Bug:
    def __str__(self):
        return 'a bug'

    def action(self):
        return 'eat it'

class FrogWorld:
    def __init__(self,name):
        print(self)
        self.player_name=name

    def __str__(self):
        return '\n\n\t------ Frog World-------'

    def make_character(self):
        return Frog(self.player_name)

    def make_obstacle(self):
        return Bug()

class Wizard:
    def __init__(self,name):
        self.name=name

    def __str__(self):
        return self.name

    def interact_with(self,obstacle):
        print('{} the Wizard battles against {} and {}!'.format(self,obstacle, obstacle.action()))

class Ork:
    def __str__(self):
        return 'an evil ork'

    def action(self):
       return 'kill it'

class WizardWorld:
    def __init__(self,name):
        print(self)
        self.player_name=name

    def __str__(self):
        return '\n\n\t------ Wizard World -------'

    def make_character(self):
        return Wizard(self.player_name)

    def make_obstacle(self):
        return Ork()

class GameEnvironment:
    def __init__(self,factory):
        self.hero =factory.make_character()
        self.obstacle =factory.make_obstacle()

    def play(self):
        self.hero.interact_with(self.obstacle)

def validata_age(name):
    try:
        age = input('Welcom {}. How old are you? '.format(name))
        age = int(age)
    except ValueError as err:
        print("Age {} is invalid, please try again...".format(age))
        return (False, age)
    return (True, age)

def main():
    name = input("Hello, What's your name? ")
    valid_input = False
    while not valid_input:
        valid_input, age = validata_age(name)
    game = FrogWorld if age < 18 else WizardWorld
    environment = GameEnvironment(game(name))
    environment.play()

if __name__=='__main__':
    main()