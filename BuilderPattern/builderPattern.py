#!/usr/bin/env python
# encoding: utf-8
__author__ = 'Zjay '
"""
@version: 3.6.3
@author: Zjay 
@license: Apache Licence 
@site: 
@software: PyCharm
@file: builderPattern.py
@time: 2018/4/16 16:58
"""


# region  FirstDemo
# MINI14 = '1.4GHz Mac mini'
#
#
# class AppleFactory:
#     class MacMini4:
#         def __init__(self):
#             self.memory = 4
#             self.hdd = 500
#             self.gpu = 'Intel HD Graphics 5000'
#
#         def __str__(self):
#             info = ('Model: {}'.format(MINI14), 'Memory: {}GB'.format(self.memory), 'Hard Disk: {}GB'.format(self.hdd),
#                     'Graphics Card: {}'.format(self.gpu))
#             return '\n'.join(info)
#
#     def build_computer(self, model):
#         if (model == MINI14):
#             return self.MacMini4()
#         else:
#             print('I do not know how to build {}'.format(model))
#
# if __name__ =='__main__':
#     afac = AppleFactory()
#     mac_mini =afac.build_computer(MINI14)
#     print(mac_mini)
# endregion

class Computer:
    def __init__(self, serial_number):
        self.serial = serial_number
        self.memory = None
        self.hdd = None
        self.gpu = None

    def __str__(self):
        info = ('SerialNumber:{}'.format(self.serial),
                'Memory: {}GB'.format(self.memory),
                'Hard Disk: {}GB'.format(self.hdd),
                'Graphics Card: {}'.format(self.gpu))
        return '\n'.join(info)


class ComputerBuilder:
    def __init__(self, serial_number):
        self.computer = Computer(serial_number)

    def configure_memory(self, amount):
        self.computer.memory = amount

    def configure_hdd(self, hdd):
        self.computer.hdd = hdd

    def configure_gpu(self, gpu):
        self.computer.gpu = gpu



class HadrWareEngineer:
    def __init__(self):
        self.builder = None

    def construct_computer(self, serialnumber, memory, hdd, gpu):
        self.builder = ComputerBuilder(serialnumber)
        self.builder.configure_memory(memory)
        self.builder.configure_hdd(hdd)
        self.builder.configure_gpu(gpu)

    @property
    def computer(self):
        return self.builder.computer


def main():
    engineer = HadrWareEngineer()
    engineer.construct_computer(serialnumber='AG23385193', hdd=500, memory=8, gpu='GeForce GTX 650 Ti')
    computer = engineer.computer
    print(computer)


if __name__ == '__main__':
    main()
